### What's New in Image Builder?
Ondřej Budai

*Senior Software Engineer @ Red Hat*

[@ondrejbudai@fosstodon.org](https://fosstodon.org/@ondrejbudai)

[osbuild.org](https://osbuild.org)
---

### What's Image Builder?

Modern tool for building operating system images!

![cockpit-composer blueprint overview](images/cockpit-composer.png)

Notes:
- From scratch, from repositories.
- No VM involved, needs root. 
- No booting!
- This talk focuses on image builder that you install on your machine. 
- There are other options, will mention later.
---
### What images can it build?

- Fedora
- CentOS Stream
- RHEL

---
### What images can it build? (2)

- KVM (libvirt, OpenStack, KubeVirt)
- AWS
- Azure
- Google Cloud
- Oracle Cloud
- VMware vSphere
---
### What images can it build? (2a)

- Installer ISO
- Container (OCI)
- OSTree artifacts (Fedora IoT/RHEL for Edge)
  - commit
  - installer
  - raw disk

Note: Some combinations are not implemented. Some might not be possible!
---
One more thing...
---
It can build *customized* images!

- custom partitioning
- extra packages
- new users
- firewalld rules
- managing systemd units
- **hardening**
- **extra files**
- **embed containers**

Notes:
- mention custom partitioning during hardening (/var/log)
---
It can take care of uploads!

- AWS EC2
- AWS S3
- Azure
- Google Cloud
- VMware vSphere
- Oracle Cloud

Notes: IB abstracts away complexity. give it a config file, IB will upload it.
---
Image Builder ships both:

- GUI for defining images visually
- CLI for fully automated pipelines
---
Just run...
```shell
dnf install -y composer-cli cockpit-compoer
systemctl enable --now osbuild-composer.socket
```
...and start building! 🚀
---

Can be also run as a web service!

- Builds golden RHEL and Fedora IoT images
- [Image building service](https://console.redhat.com/insights/image-builder) in RH Hybrid Cloud Console

Notes:
- mention API in the service

![Image builder in Red Hat Hybrid Cloud Console](./images/hms.png)<!-- .element height="70%" width="70%" -->
---
Future:

- Collaborate on using IB to build more Fedora artifacts.
- Enable the community to easily build and share customized Fedora images (think: no koji, no pungi, no rel-eng).

---
# Demo
Note: sudo firewall-cmd --list-all
