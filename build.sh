#!/bin/bash
set -euo pipefail

BLUEPRINT_NAME=$1
IMAGE_TYPE=$2

if [[ -e img ]]; then
    echo The image already exists
    exit 1
fi

TEMPDIR=$(mktemp -d)
function cleanup() {
    rm -rf "$TEMPDIR"
}
trap cleanup EXIT

echo "📥 Pushing the blueprint"
composer-cli blueprints push ${BLUEPRINT_NAME}.toml

echo "🛠 Build the image"
composer-cli --json compose start ${BLUEPRINT_NAME} ${IMAGE_TYPE} | tee "$TEMPDIR/compose_start.json"

COMPOSE_ID=$(jq -r ".[0].body.build_id" "$TEMPDIR/compose_start.json")

COUNTER=0
while true; do
    composer-cli --json compose info "${COMPOSE_ID}" | tee "$TEMPDIR/compose_info.json" > /dev/null
    COMPOSE_STATUS=$(jq -r ".[0].body.queue_status" "$TEMPDIR/compose_info.json")

    # Print a status line once per minute.
    if [ $((COUNTER%4)) -eq 0 ]; then
        echo "💤 Waiting for the compose to finish at $(date +%H:%M:%S)"
    fi

    # Is the compose finished?
    if [[ $COMPOSE_STATUS != RUNNING ]] && [[ $COMPOSE_STATUS != WAITING ]]; then
        echo "🎉 Compose finished."
        break
    fi
    sleep 15

    let COUNTER=COUNTER+1
done

composer-cli compose image "${COMPOSE_ID}" --filename img
